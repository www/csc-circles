"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
console.log("Starting CSC Circles");
const fs_1 = __importDefault(require("fs"));
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const path_1 = __importDefault(require("path"));
const APP_SOCKET_PATH = './app.sock';
const app = (0, express_1.default)();
app.use((0, morgan_1.default)('combined'));
app.use(express_1.default.json());
app.use(express_1.default.static(path_1.default.join(__dirname, '../public'))); //  "public" off of current is root
if (fs_1.default.existsSync(APP_SOCKET_PATH)) {
    fs_1.default.rmSync(APP_SOCKET_PATH);
}
let rawdata = fs_1.default.readFileSync('data.json');
//@ts-ignore
let circlesData = JSON.parse(rawdata);
let groups = [];
function processData() {
    let currentGroupIndex = 0;
    let currentGroup = { members: [] };
    circlesData.forEach(group => {
        let email = group["School email? (Primary contact for registration)"];
        let name = group["What's your full name?"];
        if (email == "") {
            groups.push(currentGroup);
            currentGroup = { members: [] };
            return;
        }
        let watiamID = email.split("@")[0];
        // console.log(email, name)
        let member = { name: name, id: watiamID };
        currentGroup.members.push(member);
    });
    groups.push(currentGroup);
}
let memberToGroup = {};
function cacheGroups() {
    groups.forEach(group => group.members.forEach(member => memberToGroup[member.id] = group));
}
processData();
cacheGroups();
console.log(memberToGroup);
console.log(JSON.stringify(groups));
app.get("/member", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const id = req.get('X-CSC-ADFS-Username');
    const firstName = req.get('X-CSC-ADFS-Firstname');
    const lastName = req.get('X-CSC-ADFS-Lastname');
    console.log(`givin ${id} group  ${JSON.stringify((_a = memberToGroup[id]) === null || _a === void 0 ? void 0 : _a.members)}`);
    res.json({ id: id, group: memberToGroup[id], name: firstName + " " + lastName });
}));
app.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.sendFile(path_1.default.join(__dirname, 'index.html'));
}));
app.listen(APP_SOCKET_PATH, () => {
    fs_1.default.chmodSync(APP_SOCKET_PATH, 0o777);
    console.log(`Listening on ${APP_SOCKET_PATH}`);
});
