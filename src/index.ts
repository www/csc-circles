console.log("Starting CSC Circles")

import fs from 'fs';
import express from 'express';
import morgan from 'morgan';
import path from 'path'
const APP_SOCKET_PATH = './app.sock';

const app = express();
app.use(morgan('combined'));
app.use(express.json())

app.use(express.static(path.join(__dirname, '../public'))); //  "public" off of current is root


if (fs.existsSync(APP_SOCKET_PATH)) {
  fs.rmSync(APP_SOCKET_PATH);
}

let rawdata = fs.readFileSync('data.json');
//@ts-ignore
let circlesData = JSON.parse(rawdata);

interface Member{
  name: string,
  id: string
}
interface Group{
  members: Member[]
}
let groups: Group[] = []

function processData(){
  let currentGroupIndex = 0
  let currentGroup: Group = {members: []}

  circlesData.forEach( group => {
    let email = group["School email? (Primary contact for registration)"]
    let name = group["What's your full name?"]

    if(email == ""){
      groups.push(currentGroup)
      currentGroup = {members: []}
      return
    }

    let watiamID = email.split("@")[0]

    // console.log(email, name)
    let member: Member = {name: name, id: watiamID}
    currentGroup.members.push(member)
  })
  groups.push(currentGroup)

}

let memberToGroup :  { [id: string] : Group;} = {}

function cacheGroups(){
  groups.forEach( group =>
    group.members.forEach(member => 
      memberToGroup[member.id] = group
      )
  )
}

processData()
cacheGroups()
console.log(memberToGroup)
console.log(JSON.stringify(groups))

app.get("/member", async (req, res) => {
  const id = req.get('X-CSC-ADFS-Username');
  const firstName = req.get('X-CSC-ADFS-Firstname');
  const lastName = req.get('X-CSC-ADFS-Lastname');

  console.log(`givin ${id} group  ${JSON.stringify(memberToGroup[id]?.members)}` )

  res.json({id:id, group: memberToGroup[id], name: firstName + " " + lastName })
});

app.get('/', async (req, res) => {
  res.sendFile(path.join(__dirname,'index.html'));
});

app.listen(APP_SOCKET_PATH, () => {
  fs.chmodSync(APP_SOCKET_PATH, 0o777);
  console.log(`Listening on ${APP_SOCKET_PATH}`);
});